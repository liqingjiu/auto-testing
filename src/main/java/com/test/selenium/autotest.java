package com.test.selenium;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.*;

public class autotest {
	
	
		
		public static void main(String[] args) { 				//main方法
	        System.setProperty("webdriver.chrome.driver", "lib/chromedriver" ); //设置chromedriver的路径 Firefox不需要 mac修改为lib/chromedriver
	        WebDriver webDriver =  new ChromeDriver(); //新建一个webDriver对象(chrome浏览器)，启动浏览器
//	        webDriver.manage().window().maximize(); //浏览器最大化
	        webDriver.get(readExecl()); //get方法打开百度	        
		}
		
		//获取Driver
		public WebDriver getDriver(String d) {
			WebDriver driver = null;
			
			//chrome
			if(d.equals("Chrome")) {
				
			}
			//火狐
			if(d.equals("火狐")) {
				
			}
			//IE
			if(d.equals("IE")) {
				
			}
			
			
			return driver;
		}
		
		//获取元素对象
		public WebElement getElement(WebDriver driver,String type,String c) {
			WebElement element = null;
			if(type.equals("xpath")) {
				element = driver.findElement(By.xpath(c));
			}
			if(type.equals("ID")) {
				element = driver.findElement(By.id(c));
			}
			
			return element;
		}
		//操作
		public void webAction(WebElement element,String a,String sendText) {
			if(a.equals("sendKey")) {
				element.sendKeys(sendText);
			}
			if(a.equals("clicj")) {
				element.click();
			}
		}
		//报告
		public void getReport() {
			
		}
		
		private static String readExecl() { 	
			String url="";
			int i;     
	        Workbook book;
	        Sheet sheet;
	        //Excel列
	        Cell cell1,cell2;
	        try { 
	        	String CaseExcel = System.getProperty("user.dir")+"/Case/case.xls";
	        	book= Workbook.getWorkbook(new File(CaseExcel));           
	        	String temp = "Case";
            sheet=book.getSheet(temp);  
	            i=1;
	            while(true)
	            {            	
	                //获取每一行的单元格 
		            	cell1=sheet.getCell(0,i);//（列，行）
	                cell2=sheet.getCell(1,i);
	                if("".equals(cell1.getContents())&&"".equals(cell2.getContents())){   //如果读取的数据为空
	                		break;
	                }else {
	                		System.out.println(cell1.getContents()+" --- "+ cell2.getContents());
	                		if(cell1.getContents().equals("url")) {
	                			url = cell2.getContents();
	                		}
					}
	                i++;
	            }
	            book.close(); 
	        }
	        catch(Exception e)  {
	        	
	        }       
	        return url;
	    }	
		
}
