package com.page;

import org.openqa.selenium.WebElement;

import com.base.DriverBase;
import com.utils.GetByByStr;

public class AddTeamPage extends BasePage {

	public AddTeamPage(DriverBase driver) {
		super(driver);
		
	}
	
	/**
	 * 获取AutoTest超链接元素(AutoTestElement)
	 */
	public WebElement getAutoTestElement(){
		return element(GetByByStr.getBy("AutoTestElement"));
	}

	/**
	 * 获取Team超链接元素(TeamElement)
	 */
	public WebElement getTeamElement(){
		return element(GetByByStr.getBy("TeamElement"));
	}
	
	/**
	 * 获取AutoTest窗体元素(AutoTestIframeElement)
	 */
	public WebElement getAutoTestIframeElement(){
		return element(GetByByStr.getBy("AutoTestIframeElement"));
	}
	
	
	/**
	 * 获取添加按钮的元素(AddButtonElement)
	 */
	public WebElement getAddButtonElement(){
		return element(GetByByStr.getBy("AddButtonElement"));
	}
	
	
	/**
	 * 获取输入窗体的元素(InputIframeElement)
	 */
	
	public WebElement getInputIframeElement(){
		return element(GetByByStr.getBy("InputIframeElement"));
	}
	
	/**
	 * 获取组名输入框的元素(TeamNameElement)
	 */
	public WebElement getTeamNameElement(){
		return element(GetByByStr.getBy("TeamNameElement"));
	}
	
	
	/**
	 * 获取描述输入框的元素(DescElement)
	 */
	public WebElement getDescElement(){
		return element(GetByByStr.getBy("DescElement"));
	}
	
	/**
	 * 获取提交按钮的元素(SubmitElement)
	 */
	public WebElement getSubmitElement(){
		return element(GetByByStr.getBy("SubmitElement"));
	}
	
	
}
