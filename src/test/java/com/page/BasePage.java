package com.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.base.DriverBase;

/**
 * 页面元素基类的封装
 * @author Administrator
 *
 */
public class BasePage {
	
	public DriverBase driver;//保证全局用同一个driver
	public BasePage(DriverBase driver){
		this.driver = driver;
	}
	
	/**
	 * 定位Element
	 * @param By by
	 * */
	public WebElement element(By by){
		WebElement element = driver.findElement(by);
		return element;
	}
	
	/**
	 * 封装点击
	 * */
	public void click(WebElement element){
		if(element !=null){
			element.click();
		}else{
			System.out.println("元素没有定位到，点击失败。");
		}
	}
	
	/**
	 * 封装输入
	 * */
	public void sendkeys(WebElement element,String value){
		if(element !=null){
			element.sendKeys(value);
		}else{
			System.out.println(element+"元素没有定位到，输入失败"+value);
		}
	}
	
	/**
	 * 判断元素是否显示
	 * */
	public boolean assertElementIs(WebElement element){
		return element.isDisplayed();
	}
	
	/**
	 * 进行窗体切换
	 */
	public void switchToIframe(WebElement element){
		driver.switchToIframe(element);
	}
	
	
}
