package com.page;

import org.openqa.selenium.WebElement;

import com.base.DriverBase;
import com.utils.GetByByStr;

public class LoginPage extends BasePage {

	public LoginPage(DriverBase driver) {
		super(driver);
		
	}

	/**
	 * 获取用户名输入框Element
	 * */
	public WebElement getUserElement(){
		return element(GetByByStr.getBy("usernameElement"));
	}
	
	/**
	 * 获取密码输入框Element
	 * */
	public WebElement getPasswordElement(){
		return element(GetByByStr.getBy("passwordElement"));
	}
	/**
	 * 获取登陆按钮element
	 * */
	public WebElement getLoginButtonElement(){
		return element(GetByByStr.getBy("loginButtonElement"));
	}
	
}
