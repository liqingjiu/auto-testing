package com.handle;

import com.base.DriverBase;
import com.page.LoginPage;

public class LoginPageHandle {

	public DriverBase driver;	//操作登录页面时也需要用到driver
	public LoginPage lp;		//需要操作的页面
	
	
	public LoginPageHandle(DriverBase driver){
		this.driver=driver;
		lp = new LoginPage(driver);	//为了保证driver是全局的同一个driver
	}
	
	/**
	 * 输入用户名
	 * */
	public void sendKeysUser(String username){
		lp.sendkeys(lp.getUserElement(), username);
	}
	
	/**
	 * 输入密码
	 * */
	public void sendKeysPassword(String password){
		lp.sendkeys(lp.getPasswordElement(), password);
	}
	/**
	 * 点击登陆
	 * */
	public void clickLoginButton(){
		lp.click(lp.getLoginButtonElement());
	}
	
	
}
