package com.handle;

import com.base.DriverBase;
import com.page.AddTeamPage;

public class AddTeamPageHandle {
	
	   public DriverBase driver; //操作登录页面时也需要用到driver
	   public AddTeamPage atp;
	   
	   public AddTeamPageHandle(DriverBase driver){
	       this.driver=driver;
	       atp = new AddTeamPage(driver);   //为了保证driver是全局的同一个driver
	   }

	   /**
	    * 点击AutoTest超链接
	    */
	   public void clickAutoTest(){
		   atp.click(atp.getAutoTestElement());
	   }
	   
	   
	   /**
		 * 点击Team超链接
		 */
	   public void clickTeam(){
		  atp.click(atp.getTeamElement());
	   }
	   
	   
	   /**
	    * 切换AutoTestIframe
	    */
	   public void switchAutoTestIframe(){
		   atp.switchToIframe(atp.getAutoTestIframeElement());
	   }
	   
	   
	   /**
	    * 点击添加按钮
	    */
	   public void clickAdd(){
		   atp.click(atp.getAddButtonElement());
	   }
	   
	   
	   /**
	    * 切换InputIframe
	    */
	   public void switchInputIframe(){
		   atp.switchToIframe(atp.getInputIframeElement());
	   }
	   
	   
	   /**
	    * 输入组名
	    */
	   public void sendkeysTeamName(String teamName){
		   atp.sendkeys(atp.getTeamNameElement(), teamName);
	   }
	   
	   
	   /**
	    * 输入备注
	    */
	   public void sendkeysDesc(String desc){
		   atp.sendkeys(atp.getDescElement(), desc);
	   }
	   
	   
	   /**
	    * 点击提交按钮
	    */
	   public void clickSubmit(){
		   atp.click(atp.getSubmitElement());
	   }
	   
}
