package com.business;

import com.base.DriverBase;
import com.handle.LoginPageHandle;

public class LoginPro {
	
	public LoginPageHandle lph;
	public DriverBase driver;
	public LoginPro(DriverBase driver){
		this.driver = driver;
		lph = new LoginPageHandle(driver);
	}
	public void login(String username,String password){
		
			lph.sendKeysUser(username);
			lph.sendKeysPassword(password);
			lph.clickLoginButton();
		
	}

}
