package com.business;

import com.base.DriverBase;
import com.handle.AddTeamPageHandle;

public class AddTeamPro {

	public AddTeamPageHandle atph;
	public DriverBase driver;
	
	public AddTeamPro(DriverBase driver){
		 this.driver = driver;
		 atph = new AddTeamPageHandle(driver);
	}
	
		
	public void addTeam(String teamName,String desc){
		atph.clickAutoTest();
		atph.clickTeam();
		atph.switchAutoTestIframe();
		atph.clickAdd();
		atph.switchInputIframe();
		atph.sendkeysTeamName(teamName);
		atph.sendkeysDesc(desc);
		atph.clickSubmit();
		
	}
}
