package com.testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.base.DriverBase;
import com.business.LoginPro;


public class LoginTest extends CaseBase {
	public DriverBase driver;
	public LoginPro loginpro;
	
	public LoginTest(){
		this.driver = InitDriver("chrome");
		driver.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		loginpro = new LoginPro(driver);
	}
	@Test
	public void getLoginHome(){
		String url = "http://115.159.25.34:8090/login";
		driver.get(url);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		driver.maximize();
	}
	@Test(dependsOnMethods={"getLoginHome"})
	public void testLogin(){
		String username="WSC110";
		String password="111111";
		loginpro.login(username,password);
	}
}
