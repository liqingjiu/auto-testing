package com.testCase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.base.DriverBase;
import com.business.AddTeamPro;
import com.business.LoginPro;


public class AddTeamTest extends CaseBase {

	public DriverBase driver;
	public AddTeamPro addTeamPro;
	
	public LoginPro loginpro;	//试试
	
	public AddTeamTest(){
		
		this.driver = InitDriver("chrome");
		driver.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		loginpro = new LoginPro(driver);	//试试
		addTeamPro = new AddTeamPro(driver);
	}
	
	//试试
	@Test
	public void getLoginHome(){
		String url = "http://115.159.25.34:8090/login";
		driver.get(url);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		driver.maximize();
	}
	@Test(dependsOnMethods={"getLoginHome"})
	public void testLogin(){
		String username="WSC110";
		String password="111111";
		loginpro.login(username,password);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	//试试
	
	@Test(dependsOnMethods={"testLogin"})
	public void testAdd(){
		String teamName = "小杰克军团";
		String desc = "clever";
		addTeamPro.addTeam(teamName, desc);
	}
}
