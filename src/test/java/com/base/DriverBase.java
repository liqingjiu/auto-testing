package com.base;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Driver基类的封装  (可看成后面调用时用driverbase封装webDriver)
 * @author Administrator
 *
 */
public class DriverBase {
	
	public WebDriver driver;//保证全局使用同一个driver
	
	/**
	 * 该构造方法用来初始化driver
	 * @param browser
	 */
	public DriverBase(String browser){
		SelectDriver selectDriver =new SelectDriver();
		this.driver = selectDriver.driverName(browser);
	}
	
	public void stop(){
		System.out.println("stop webdriver");
		driver.close();
	}
	
	/**
	 * 封装Element方法(为了使后面能直接调用)
	 * */
	public WebElement findElement(By by){
		WebElement element = driver.findElement(by);
		return element;
	}
	/**
	 * 封装定位一组elements的方法(为了使后面能直接调用)
	 * */
	public List<WebElement> findElements(By by){
		return driver.findElements(by);
	}
	
	/**
     * get封装
     * */
    
    public void get(String url){
 	   driver.get(url);
    }
	
	/**
	 * 最大化
	 */
	
	public void maximize(){
		driver.manage().window().maximize();
	}
    
	/**
	 * 切换iframe
	 */
public void switchToIframe(WebElement element){
		driver.switchTo().frame(element);
	}
}
