package com.utils;

import org.openqa.selenium.By;



public class GetByByStr {

	public static By getBy(String aliasEle){
		ProUtils properties = new ProUtils("element.properties");
		String locator = properties.getPro(aliasEle);
		String locatorType = locator.split(">")[0];
		String locatorValue = locator.split(">")[1];
		
		if("id".equals(locatorType)){
			return By.id(locatorValue);
		}else if("name".equals(locatorType)){
			return By.name(locatorValue);
		}else if ("classname".equals(locatorType)){
			return By.className(locatorValue);
		}else{
			return By.xpath(locatorValue);
		}
	}
}
